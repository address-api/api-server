package in.silentsudo.addressapi.validator;

import in.silentsudo.addressapi.persistence.entities.State;
import in.silentsudo.addressapi.services.StateService;
import in.silentsudo.addressapi.validator.annotations.StateId;
import lombok.extern.slf4j.Slf4j;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Optional;

@Slf4j
public class StateIdValidator implements ConstraintValidator<StateId, Long> {
    private final StateService stateService;

    public StateIdValidator(StateService stateService) {
        this.stateService = stateService;
    }

    @Override
    public boolean isValid(Long stateId, ConstraintValidatorContext constraintValidatorContext) {
        final Optional<State> stateOptional = stateService.findStateById(stateId);
        log.info("State for {} found {}", stateId, stateOptional.isPresent());
        return stateOptional.isPresent();
    }
}
