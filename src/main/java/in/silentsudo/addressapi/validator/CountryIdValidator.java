package in.silentsudo.addressapi.validator;

import in.silentsudo.addressapi.persistence.entities.Country;
import in.silentsudo.addressapi.services.CountryService;
import in.silentsudo.addressapi.validator.annotations.CountryId;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Optional;

@Slf4j
public class CountryIdValidator implements ConstraintValidator<CountryId, Long> {

    @Autowired
    private CountryService countryService;

    @Override
    public boolean isValid(Long countryId, ConstraintValidatorContext constraintValidatorContext) {
        final Optional<Country> country = countryService.findCountryById(countryId);
        log.info("Country for {} found {}", countryId, country.isPresent());
        return country.isPresent();
    }
}
