package in.silentsudo.addressapi.controllers;

import in.silentsudo.addressapi.persistence.entities.City;
import in.silentsudo.addressapi.services.CityService;
import in.silentsudo.addressapi.validator.annotations.StateId;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Slf4j
@Validated
@RestController
@RequestMapping("/city")
public class CityController {

    private final CityService cityService;

    @Autowired
    public CityController(CityService cityService) {
        this.cityService = cityService;
    }

    @GetMapping(value = "all")
    public Iterable<City> getAllStates(@Valid @StateId @RequestParam Long stateId) {
        log.info("StateId id requested {}", stateId);
        return cityService.getCitiesByState(stateId);
    }
}
