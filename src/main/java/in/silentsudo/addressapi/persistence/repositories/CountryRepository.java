package in.silentsudo.addressapi.persistence.repositories;

import in.silentsudo.addressapi.persistence.entities.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CountryRepository extends JpaRepository<Country, Long> {

}
