CREATE TABLE IF NOT EXISTS `countries` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `sortname` varchar(3) NOT NULL,
  `name` varchar(150) NOT NULL,
  `phonecode` bigint(11) NOT NULL,
  is_active bit(1) DEFAULT b'1',
    created_by varchar(255) DEFAULT NULL,
    creation_date timestamp NULL DEFAULT NULL,
    last_modified_by varchar(255) DEFAULT NULL,
    last_modified_date timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `states` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `country_id` bigint(11) NOT NULL DEFAULT '1',
  is_active bit(1) DEFAULT b'1',
  created_by varchar(255) DEFAULT NULL,
    creation_date timestamp NULL DEFAULT NULL,
    last_modified_by varchar(255) DEFAULT NULL,
    last_modified_date timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  constraint fk_states_country foreign key (country_id) references countries(id)
);

CREATE TABLE IF NOT EXISTS `cities` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `state_id` bigint(11) NULL,
  is_active bit(1) DEFAULT b'1',
  created_by varchar(255) DEFAULT NULL,
    creation_date timestamp NULL DEFAULT NULL,
    last_modified_by varchar(255) DEFAULT NULL,
    last_modified_date timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
);