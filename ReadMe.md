# BackEnd

## Create Env Var
```sh
MYSQL_HOST=
MYSQL_PORT=
MYSQL_SECRET=
```

## Create Database
```sql
create database address_api_db;
```

## Clone and run project
```
./mvnw spring-boot:run
```